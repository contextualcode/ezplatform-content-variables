<?php

namespace ContextualCode\EzPlatformContentVariablesBundle\Service\Handler;

use ContextualCode\EzPlatformContentVariablesBundle\Entity\Collection as CollectionEntity;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectRepository;

class Collection extends Handler
{
    protected function getRepository(ManagerRegistry $doctrine): ObjectRepository
    {
        return $doctrine->getRepository(CollectionEntity::class);
    }
}
